
const Logger = (url, { log = false, info = false, warn = false, error = true, metadata }) => {
    if(url === undefined) {
        window.console.error('`url` is required')
        return
    }

    const { console, onerror } = window

    const save = (priority, message) => {
        const full_message = {
            priority,
            created: new Date(),
            host: window.location,
            details: message
        }

        if(metadata)
            full_message.metadata = metadata()

        fetch(url, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(full_message)
        }).catch(error => {
            console.error(`Unable to save log - ${error}`)
            console.info(message)
        })
    }

    window.console = {
        ...console,
        log: log ? function(){
            console.log.apply(arguments)
            save('log', arguments)
        } : console.log,
        info: info ? function () {
            console.info.apply(arguments)
            save('info', arguments)
        } : console.info,
        warn: warn ? function () {
            console.warn.apply(arguments)
            save('warn', arguments)
        } : console.warn,
        error: error ? function () {
            console.error.apply(arguments)
            save('error', arguments)
        } : console.error
    }

    window.onerror = (
        message,
        source,
        lineno,
        colno,
        error
    ) => {
        if(onerror)
            onerror(message, source, lineno, colno, error)
        save({message, source, lineno, colno, error})
    }
}

export default Logger
