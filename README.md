# Client Side Logging

Send logs from client side back to a preset url.

## Usage
```javascript
import { Logger } from 'client-side-logging'

Logger({ url: 'example.com/savelogs' })
```


